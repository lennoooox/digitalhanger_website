export const inputStyles = {
  default: {
    color: 'rgba(25,25,28,0.2)',
    borderColor: 'rgba(160,160,192,0.2)',
  },
  valid: {
    color: 'rgba(148,222,225,1)',
    borderColor: 'rgba(148,222,225,1)',
  },
  invalid: {
    color: 'rgba(255,0,0,0.5)',
    borderColor: 'rgba(255,0,0,1)',
  },
};

export const questionStyles = {
  hidden: {
    to: {
      opacity: 0,
      transform: 'translateX(-50px)',
    },
  },
  visible: async next => {
    await next({ transform: 'translateX(50px)', opacity: 0 });
    await next({ transform: 'translateX(0px)', opacity: 1 });
  },
  /*visible: {
    from: {
      opacity: 0,
      transform: 'translateX(50px)',
    },
    to: {
      opacity: 1,
      transform: 'translateX(0px)',
    },
    native: true,
  },*/
};
