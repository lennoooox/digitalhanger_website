import React from 'react';
import PropTypes from 'prop-types';
import uniqueId from 'lodash/uniqueId';
import { connect } from 'react-redux';
import { Keyframes, animated, config } from 'react-spring';
// LOCALIZATION
import createLocalizedStrings from '../../../utils/createLocalizedStrings';
import localization from '../localization';
// SELECTORS
import { getLanguage } from '../../App/selectors';
// STYLES
import stylesCSS from './main.module.scss';
import { toggleServiceSelection } from '../actions';
import { getCurrentQuestionId, getQuestionData } from '../selectors';

const AnimatedServiceCard = Keyframes.Spring({
  default: {
    background:
      'linear-gradient(45deg, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 100%)',
    fontWeight: 300,
    color: '#000',
  },
  selected: {
    background:
      'linear-gradient(45deg, rgba(255,196,61,1) 0%, rgba(255,161,124,1) 100%)',
    fontWeight: 600,
    color: '#fff',
  },
});

class ServicesPicker extends React.Component {
  render() {
    const {
      styles,
      currentQuestionId,
      getQuestionData,
      toggleServiceSelection,
      language,
    } = this.props;

    const strings = createLocalizedStrings(language, localization);
    const services = [
      strings.development,
      strings.design,
      strings.consultation,
    ];

    const questionData = getQuestionData(currentQuestionId);

    return (
      <animated.div style={styles} className={stylesCSS.servicesHolder}>
        {services.map((service, index) => (
          <AnimatedServiceCard
            native
            config={config.stiff}
            state={
              questionData.value.indexOf(index) > -1 ? 'selected' : 'default'
            }
          >
            {props => (
              <animated.div
                key={uniqueId('service-item-')}
                className={stylesCSS.serviceCard}
                onClick={() => toggleServiceSelection(index)}
                style={props}
              >
                {service}
              </animated.div>
            )}
          </AnimatedServiceCard>
        ))}
      </animated.div>
    );
  }
}

ServicesPicker.propTypes = {
  // mapStateToProps
  language: PropTypes.string.isRequired,
  currentQuestionId: PropTypes.string.isRequired,
  getQuestionData: PropTypes.func.isRequired,
  // mapDispatchToProps
  toggleServiceSelection: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    language: getLanguage(state),
    currentQuestionId: getCurrentQuestionId(state),
    getQuestionData: id => getQuestionData(state, id),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    toggleServiceSelection: index => dispatch(toggleServiceSelection(index)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ServicesPicker);
