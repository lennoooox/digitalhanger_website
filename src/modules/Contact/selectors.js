import { NAMESPACE } from './constants';

export const getIsTransitioning = state => state[NAMESPACE].isTransitioning;
export const getQuestionData = (state, questionId) =>
  state[NAMESPACE][questionId] || { value: '', valid: false };
export const getCurrentQuestionId = state => state[NAMESPACE].currentQuestionId;
export const getSendingState = state => state[NAMESPACE].sendingState;
