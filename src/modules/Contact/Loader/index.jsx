import React from 'react';
import { animated } from 'react-spring';
// STYLES
import styles from './main.module.scss';

const Loader = props => {
  const { propsQuestion } = props;

  return <animated.div style={propsQuestion} className={styles.spinner} />;
};

export default Loader;
