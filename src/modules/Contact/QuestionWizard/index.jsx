import React from 'react';
import { connect } from 'react-redux';
import { Keyframes, animated } from 'react-spring';
import { questions } from '../constants';
// LOCALIZATION
import localization from '../localization';
import createLocalizedStrings from '../../../utils/createLocalizedStrings';
// SELECTORS
import { getLanguage } from '../../App/selectors';
import {
  getCurrentQuestionId,
  getIsTransitioning,
  getQuestionData,
  getSendingState,
} from '../selectors';
// ACTIONS
import {
  showQuestionWithId,
  submitForm,
  updateQuestionValue,
  validateQuestionValue,
} from '../actions';
import { inputStyles } from '../springStyles';
import styles from './main.module.scss';

const AnimatedInput = Keyframes.Spring(inputStyles);

class QuestionWizard extends React.Component {
  onInputChange = e => {
    const {
      updateQuestionValue,
      validateQuestionValue,
      isTransitioning,
    } = this.props;
    const value = e.target.value;

    if (!isTransitioning) {
      updateQuestionValue(value);
      value.length >= 5 && validateQuestionValue(value);
    }
  };

  navigateToNextQuestion = e => {
    e.preventDefault();
    const {
      currentQuestionId,
      getQuestionData,
      isTransitioning,
      showQuestionWithId,
    } = this.props;
    const questionData = getQuestionData(currentQuestionId);
    const nextQuestionId = questions[currentQuestionId].nextStep;

    if (
      (questionData.valid === true && !isTransitioning) ||
      (currentQuestionId === 'services' && questionData.value.length >= 1)
    )
      showQuestionWithId(nextQuestionId);
  };

  navigateToPreviousQuestion = e => {
    e.preventDefault();
    const {
      currentQuestionId,
      isTransitioning,
      showQuestionWithId,
    } = this.props;
    const previousStepId = questions[currentQuestionId].prevStep;

    if (!isTransitioning) showQuestionWithId(previousStepId);
  };

  submitForm = e => {
    e.preventDefault();
    const { language, getQuestionData, submitForm } = this.props;
    const strings = createLocalizedStrings(language, localization);
    const formData = {};

    Object.keys(questions).forEach(key => {
      let value = getQuestionData(key).value;

      if (key === 'services') {
        const items = value.forEach(i => i.toString().replace(i, strings));
        value = value.join(', ');
      }

      Object.assign(formData, {
        [key]: value,
      });
    });

    submitForm(formData);
  };

  onKeyPress = e => {
    if (e.key === 'Enter') {
      e.preventDefault();
      this.navigateToNextQuestion(e);
    }
  };

  componentDidMount() {
    window.addEventListener('keypress', this.onKeyPress);
  }

  componentWillUnmount() {
    window.removeEventListener('keypress', this.onKeyPress);
  }

  render() {
    const {
      propsQuestion,
      currentQuestionId,
      getQuestionData,
      language,
      sendingState,
    } = this.props;
    const strings = createLocalizedStrings(language, localization);
    const question = questions[currentQuestionId];
    const questionData = getQuestionData(currentQuestionId);
    const indexOfQuestion = Object.keys(questions).indexOf(currentQuestionId);
    const SpecialComp = question.component || <React.Fragment />;
    const inputValid =
      questionData.value.length >= 1
        ? questionData.valid === true
          ? 'valid'
          : questionData.valid === 'unvalidated'
          ? 'default'
          : 'invalid'
        : 'default';

    return (
      <form id={styles.form}>
        {indexOfQuestion === 0 ? (
          <animated.h1 style={propsQuestion}>{strings.contact}</animated.h1>
        ) : (
          sendingState === 'unsent' && (
            <animated.button
              id={styles.prevButton}
              style={propsQuestion}
              onClick={this.navigateToPreviousQuestion}
            >
              {strings.prevButtonLabel}
            </animated.button>
          )
        )}
        {currentQuestionId !== 'submitting' && (
          <animated.h2 id={styles.fieldTitle} style={propsQuestion}>
            {strings[question.questionTitle]}
          </animated.h2>
        )}
        {question.type !== 'special' ? (
          <AnimatedInput native state={inputValid}>
            {propsInput => (
              <animated.input
                id={styles.fieldInput}
                type={question.type}
                value={questionData.value}
                onChange={this.onInputChange}
                placeholder={strings[question.questionPlaceholder]}
                style={{ ...propsInput, ...propsQuestion }}
              />
            )}
          </AnimatedInput>
        ) : currentQuestionId !== 'submitting' ? (
          <SpecialComp styles={propsQuestion} />
        ) : (
          <div className={styles.spinner} />
        )}
        <animated.button
          id={styles.nextButton}
          style={propsQuestion}
          onClick={
            indexOfQuestion === Object.keys(questions).length - 1
              ? this.submitForm
              : this.navigateToNextQuestion
          }
        >
          {indexOfQuestion === Object.keys(questions).length - 1
            ? strings.submitButtonLabel
            : strings.nextButtonLabel}
        </animated.button>
        <div id={styles.counter} style={propsQuestion}>{`0${Object.keys(
          questions
        ).indexOf(currentQuestionId) + 1}/0${
          Object.keys(questions).length
        }`}</div>
      </form>
    );
  }
}

function mapStateToProps(state) {
  return {
    language: getLanguage(state),
    currentQuestionId: getCurrentQuestionId(state),
    isTransitioning: getIsTransitioning(state),
    getQuestionData: id => getQuestionData(state, id),
    sendingState: getSendingState(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateQuestionValue: value => dispatch(updateQuestionValue(value)),
    validateQuestionValue: value => dispatch(validateQuestionValue(value)),
    showQuestionWithId: value => dispatch(showQuestionWithId(value)),
    submitForm: data => dispatch(submitForm(data)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuestionWizard);
