import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Keyframes } from 'react-spring';
import { questions } from './constants';
// ACTIONS
import {
  toggleFooter,
  toggleNavigationDark,
  toggleNavigationFullWidth,
} from '../App/actions';
import { generateDefaultState, showQuestionWithId } from './actions';
// SELECTORS
import { getIsTransitioning, getSendingState } from './selectors';
import {
  getFooterVisible,
  getLanguage,
  getNavigationDark,
  getNavigationFullWidth,
} from '../App/selectors';
// UI
import QuestionWizard from './QuestionWizard';
import SuccessfullySent from './SuccessfullySent';
import Loader from './Loader';
// STYLES
import { questionStyles } from './springStyles';
import styles from './main.module.scss';

const AnimatedQuestion = Keyframes.Spring(questionStyles);

class Contact extends React.Component {
  componentWillMount() {
    this.props.showQuestionWithId(Object.keys(questions)[0]);
    this.props.generateDefaultState();
  }

  componentDidMount() {
    const {
      footerVisible,
      navigationDark,
      navigationFullWidth,
      toggleFooter,
      toggleNavigationDark,
      toggleNavigationFullWidth,
    } = this.props;

    window.onkeypress = this.onKeyPress;

    footerVisible && toggleFooter(false);
    !navigationDark && toggleNavigationDark(true);
    navigationFullWidth && toggleNavigationFullWidth(false);
  }

  render() {
    const { sendingState, isTransitioning } = this.props;
    let Output;

    switch (sendingState) {
      case 'sent':
        Output = SuccessfullySent;
        break;
      case 'error':
        Output = Loader;
        break;
      case 'sending':
        Output = Loader;
        break;
      default:
        Output = QuestionWizard;
        // Output = SuccessfullySent;
        break;
    }

    return (
      <section id={styles.contact}>
        <div className="wrapper">
          <div id={styles.content}>
            <AnimatedQuestion state={isTransitioning ? 'hidden' : 'visible'}>
              {props => <Output propsQuestion={props} />}
            </AnimatedQuestion>
          </div>
        </div>
      </section>
    );
  }
}

Contact.propTypes = {
  // mapStateToProps
  footerVisible: PropTypes.bool.isRequired,
  navigationDark: PropTypes.bool.isRequired,
  navigationFullWidth: PropTypes.bool.isRequired,
  language: PropTypes.string.isRequired,
  // mapDispatchToProps
  toggleFooter: PropTypes.func.isRequired,
  toggleNavigationDark: PropTypes.func.isRequired,
  toggleNavigationFullWidth: PropTypes.func.isRequired,
  generateDefaultState: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  language: getLanguage(state),
  footerVisible: getFooterVisible(state),
  navigationDark: getNavigationDark(state),
  navigationFullWidth: getNavigationFullWidth(state),
  sendingState: getSendingState(state),
  isTransitioning: getIsTransitioning(state),
});

const mapDispatchToProps = dispatch => ({
  toggleFooter: value => dispatch(toggleFooter(value)),
  toggleNavigationDark: value => dispatch(toggleNavigationDark(value)),
  toggleNavigationFullWidth: value =>
    dispatch(toggleNavigationFullWidth(value)),
  generateDefaultState: () => dispatch(generateDefaultState()),
  showQuestionWithId: id => dispatch(showQuestionWithId(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contact);
