import ServicesPicker from './ServicesPicker';

export const NAMESPACE = 'contact';

export const questions = {
  name: {
    questionTitle: 'nameTitle',
    questionPlaceholder: 'namePlaceholder',
    type: 'text',
    regex: /^[a-zA-Z ,.'-]+$/gi,
    nextStep: 'company',
  },
  company: {
    questionTitle: 'companyTitle',
    questionPlaceholder: 'companyPlaceholder',
    type: 'text',
    regex: /^[A-Z]([a-zA-Z0-9]|[- @\.#&!])*$/gi,
    nextStep: 'email',
    prevStep: 'name',
  },
  email: {
    questionTitle: 'emailTitle',
    questionPlaceholder: 'emailPlaceholder',
    type: 'email',
    regex: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/gi,
    nextStep: 'services',
    prevStep: 'company',
  },
  services: {
    questionTitle: 'servicesTitle',
    type: 'special',
    component: ServicesPicker,
    nextStep: 'message',
    prevStep: 'email',
  },
  message: {
    questionTitle: 'messageTitle',
    questionPlaceholder: 'messagePlaceholder',
    type: 'text',
    nextStep: 'name',
    prevStep: 'services',
  },
};
