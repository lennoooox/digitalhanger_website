import {
  GENERATE_DEFAULT_STATE,
  UPDATE_QUESTION_VALUE,
  VALIDATE_QUESTION_VALUE,
  TOGGLE_TRANSITIONING,
  SET_QUESTION,
  UPDATE_SENDING_STATE,
} from './actions';
import { questions } from './constants';

const initialState = {
  isTransitioning: false,
  sendingState: 'unsent',
  currentQuestionId: Object.keys(questions)[0],
};

const ContactReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_QUESTION:
      return Object.assign({}, state, {
        currentQuestionId: action.payload,
      });
    case TOGGLE_TRANSITIONING:
      return Object.assign({}, state, {
        isTransitioning: action.payload,
      });
    case UPDATE_SENDING_STATE:
      return Object.assign({}, state, {
        sendingState: action.payload,
      });
    case UPDATE_QUESTION_VALUE:
      return Object.assign({}, state, {
        [state.currentQuestionId]: Object.assign(
          state[state.currentQuestionId],
          {
            value: action.payload.value,
            valid: action.payload.valid,
          }
        ),
      });
    case VALIDATE_QUESTION_VALUE:
      return Object.assign({}, state, {
        [state.currentQuestionId]: Object.assign(
          state[state.currentQuestionId],
          {
            valid: action.payload,
          }
        ),
      });
    case GENERATE_DEFAULT_STATE:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
};

export default ContactReducer;
