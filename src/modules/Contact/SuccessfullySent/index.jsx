import React from 'react';
import { connect } from 'react-redux';
import routes from '../../../routes';
// LOCALIZATION
import createLocalizedStrings from '../../../utils/createLocalizedStrings';
import localization from '../localization';
// SELECTORS
import { getLanguage } from '../../App/selectors';
// UI
import { Link } from 'react-router-dom';
// STYLES
import styles from './main.module.scss';

const SuccessfullySent = props => {
  const { language, propsQuestion } = props;
  const strings = createLocalizedStrings(language, localization);

  return (
    <div style={propsQuestion}>
      <h1>{strings.contact}</h1>
      <h2 id={styles.headline}>{strings.successHeadline}</h2>
      <Link className={styles.link} to={routes.projects.main}>
        {strings.references}
      </Link>
    </div>
  );
};

const mapStateToProps = state => ({
  language: getLanguage(state),
});

export default connect(mapStateToProps)(SuccessfullySent);
