import * as EmailJS from 'emailjs-com';
import { questions } from './constants';

import { getCurrentQuestionId, getQuestionData } from './selectors';

export const GENERATE_DEFAULT_STATE = 'CONTACT/GENERATE_DEFAULT_STATE';
export const UPDATE_QUESTION_VALUE = 'CONTACT/UPDATE_QUESTION_VALUE';
export const VALIDATE_QUESTION_VALUE = 'CONTACT/VALIDATE_QUESTION_VALUE';
export const TOGGLE_TRANSITIONING = 'CONTACT/TOGGLE_TRANSITIONING';
export const SET_QUESTION = 'CONTACT/SET_QUESTION';
export const UPDATE_SENDING_STATE = 'CONTACT/UPDATE_SENDING_STATE';

export const generateDefaultState = () => dispatch => {
  const defaultState = {};

  for (let question in questions) {
    Object.assign(defaultState, {
      [question]: {
        value: question === 'services' ? [] : '',
        valid: question === 'services' ? false : 'unvalidated',
      },
    });
  }

  return dispatch({ type: GENERATE_DEFAULT_STATE, payload: defaultState });
};

export const updateSendingState = state => dispatch => {
  dispatch({ type: TOGGLE_TRANSITIONING, payload: true });

  return setTimeout(() => {
    dispatch({
      type: UPDATE_SENDING_STATE,
      payload: state,
    });
    dispatch({ type: TOGGLE_TRANSITIONING, payload: false });
  }, 500);
};

export const submitForm = data => dispatch => {
  dispatch(updateSendingState('sending'));

  EmailJS.send(
    'mailgun',
    'kiai-agency',
    data,
    process.env.REACT_APP_EMAILJS
  ).then(
    () => {
      dispatch(updateSendingState('sent'));
    },
    () => {
      dispatch(updateSendingState('error'));
    }
  );
};

export const toggleServiceSelection = index => (dispatch, getState) => {
  const selectedServices = getQuestionData(getState(), 'services').value;
  const indexOfService = selectedServices.indexOf(index);

  if (indexOfService > -1) {
    selectedServices.splice(indexOfService, 1);
  } else {
    selectedServices.push(index);
  }

  return dispatch(updateQuestionValue(selectedServices));
};

export const showQuestionWithId = id => (dispatch, getState) => {
  const currentQuestionId = getCurrentQuestionId(getState());
  const currentQuestionData = getQuestionData(getState(), currentQuestionId);

  if (currentQuestionData.valid) {
    dispatch({ type: TOGGLE_TRANSITIONING, payload: true });

    return setTimeout(() => {
      dispatch({ type: SET_QUESTION, payload: id });
      dispatch({ type: TOGGLE_TRANSITIONING, payload: false });
    }, 500);
  }
};

export const updateQuestionValue = value => (dispatch, getState) => {
  const currentQuestionId = getCurrentQuestionId(getState());
  const currentQuestionData = getQuestionData(getState(), currentQuestionId);

  let payload = {
    value,
    valid: currentQuestionData.valid,
  };

  if (value.length < 5) {
    payload = {
      value,
      valid: 'unvalidated',
    };
  }

  return dispatch({ type: UPDATE_QUESTION_VALUE, payload });
};

export const validateQuestionValue = value => (dispatch, getState) => {
  const currentQuestionId = getCurrentQuestionId(getState());
  const valid = !!value.match(questions[currentQuestionId].regex);

  return dispatch({
    type: VALIDATE_QUESTION_VALUE,
    payload: valid,
  });
};
