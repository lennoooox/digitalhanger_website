export default {
    en: {
        headlineShop: 'Online-Shops',
        paragraphShop: 'Our talented developer collective creates digital experiences for people through innovative and novel technologies.',
        headlineMarketing: 'Marketing',
        paragraphMarketing: 'Our talented developer collective creates digital experiences for people through innovative and novel technologies.',
        headlineLeads: 'Leads',
        paragraphLeads: 'Our talented developer collective creates digital experiences for people through innovative and novel technologies.',
        linkText: 'Learn more'
    },
    de: {
        headlineShop: 'Online-Shops',
        paragraphShop: 'Unser talentiertes Entwicklerkollektiv schafft durch innovative und neuartige Technologien digitale Erlebnisse für Menschen.',
        headlineMarketing: 'Marketing',
        paragraphMarketing: 'Unser talentiertes Entwicklerkollektiv schafft durch innovative und neuartige Technologien digitale Erlebnisse für Menschen.',
        headlineLeads: 'Leads',
        paragraphLeads: 'Unser talentiertes Entwicklerkollektiv schafft durch innovative und neuartige Technologien digitale Erlebnisse für Menschen.',
        linkText: 'Mehr erfahren'
    },
};
