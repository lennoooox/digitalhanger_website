import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom'

// LOCALIZATION
import createLocalizedStrings from '../../../utils/createLocalizedStrings';
import localization from './localization';

// STYLES
import styles from './main.module.scss';
import {getLanguage} from '../../App/selectors';
import routes from '../../../routes';

const Details = props => {
    const {language} = props;
    const strings = createLocalizedStrings(language, localization);

    return (
        <section>
            <div id={styles.content}>
                <Link to={routes.services.single.main.replace(':id', '1')} componentclass="p" id={styles.box}>
                    <p id={styles.quote}>01</p>
                    <p id={styles.headline}>{strings.headlineShop}</p>
                    <p id={styles.paragraph}>{strings.paragraphShop}</p>
                    <p id={styles.link}>{strings.linkText}</p>
                </Link>
                <Link to={routes.services.single.main.replace(':id', '1')} componentclass="p" id={styles.box}>
                    <p id={styles.quote}>02</p>
                    <p id={styles.headline}>{strings.headlineMarketing}</p>
                    <p id={styles.paragraph}>{strings.paragraphMarketing}</p>
                    <p id={styles.link}>{strings.linkText}</p>
                </Link>
                <Link to={routes.services.single.main.replace(':id', '1')} componentclass="p" id={styles.box}>
                    <p id={styles.quote}>03</p>
                    <p id={styles.headline}>{strings.headlineLeads}</p>
                    <p id={styles.paragraph}>{strings.paragraphLeads}</p>
                    <p id={styles.link}>{strings.linkText}</p>
                </Link>
            </div>
        </section>
    );
};

Details.propTypes = {
    // mapStateToProps
    language: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
    language: getLanguage(state),
});

const connected = connect(mapStateToProps)(Details);
export default connected;