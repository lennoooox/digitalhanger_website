import * as constants from './constants';
import {
  SHOW_NEXT_HEADLINE,
  SHOW_HEADLINE,
  HIDE_HEADLINE,
  SHOW_NEXT_TESTIMONIAL,
  SHOW_TESTIMONIAL,
  HIDE_TESTIMONIAL,
} from './actions';

const initialState = {
  headlineHidden: false,
  headlines: constants.heroHeadlines,
  headlineToShow: constants.heroHeadlines[0],
  testimonialHidden: false,
  testimonials: constants.testimonials,
  testimonialToShow: constants.testimonials[0],
};

export const HomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_NEXT_HEADLINE:
      return Object.assign({}, state, {
        headlineToShow: action.payload,
      });
    case SHOW_NEXT_TESTIMONIAL:
      return Object.assign({}, state, {
        testimonialToShow: action.payload,
      });
    case SHOW_HEADLINE:
      return Object.assign({}, state, {
        headlineHidden: false,
      });
    case SHOW_TESTIMONIAL:
      return Object.assign({}, state, {
        testimonialHidden: false,
      });
    case HIDE_HEADLINE:
      return Object.assign({}, state, {
        headlineHidden: true,
      });
    case HIDE_TESTIMONIAL:
      return Object.assign({}, state, {
        testimonialHidden: true,
      });
    default:
      return state;
  }
};

export default HomeReducer;
