import AlexanderNelson from './quotes/images/alexander-nelson.jpg';
import AlexanderNelsonRetina from './quotes/images/alexander-nelson@2x.jpg';
import VolkerStuetzinger from './quotes/images/volker-stuetzinger.jpg';
import VolkerStuetzingerRetina from './quotes/images/volker-stuetzinger@2x.jpg';
import StefanSimak from './quotes/images/stefan-simak.jpg';
import StefanSimakRetina from './quotes/images/stefan-simak@2x.jpg';

export const NAMESPACE = 'home';

export const heroHeadlines = [
  'headlineDigitalSolutions',
  'headlineEmotions',
  'headlineDigitalPlayground',
];

export const testimonials = [
  {
    content:
      'Effiziente Umsetzung und modernste Technologien zeichnen die Zusammenarbeit zwischen Leica Sportoptik und KIAI aus. Schnell wurde aus anfänglich einer Projektanfrage eine ausgedehnte Partnerschaft, in der wir gemeinsam digitale Innovationen voran treiben.',
    author: 'Alexander Nelson',
    position: 'Product Manager, Leica Camera AG',
    avatar: AlexanderNelson,
    avatarRetina: AlexanderNelsonRetina,
  },
  {
    content:
      'Als wir das AEG eBike entwickelt haben, wussten wir, dass wir neue Wege gehen mussten. Für uns war es da nur naheliegend, mit KIAI zusammen zu arbeiten, denn wir wussten, dass sie unseren Anspruch an Innovation teilen – und das haben sie mit der App bewiesen.',
    author: 'Volker Stützinger',
    position: 'Sales- & Marketingmanager, AEG eBike',
    avatar: VolkerStuetzinger,
    avatarRetina: VolkerStuetzingerRetina,
  },
  {
    content:
      'We could not be more pleased with the results of our cooperation with KIAI to build our new Canyon Factory Racing portal. The site is dynamic and displays all of the content in a clean and easy-to-use format – and KIAI was quick to put the website together.',
    author: 'Stefan Simak',
    position: 'Creative Producer, Canyon Bicycles',
    avatar: StefanSimak,
    avatarRetina: StefanSimakRetina,
  },
];
