import {
  getHeadlines,
  getHeadlineToShow,
  getTestimonials,
  getTestimonialToShow,
} from './selectors';

export const SHOW_NEXT_HEADLINE = 'HOME/SHOW_NEXT_HEADLINE';
export const HIDE_HEADLINE = 'HOME/HIDE_HEADLINE';
export const SHOW_HEADLINE = 'HOME/SHOW_HEADLINE';
export const SHOW_NEXT_TESTIMONIAL = 'HOME/SHOW_NEXT_TESTIMONIAL';
export const HIDE_TESTIMONIAL = 'HOME/HIDE_TESTIMONIAL';
export const SHOW_TESTIMONIAL = 'HOME/SHOW_TESTIMONIAL';

export const showNextHeadline = () => (dispatch, getState) => {
  const headlines = getHeadlines(getState());
  const currentHeadline = getHeadlineToShow(getState());
  const currentHeadlineIndex = headlines.indexOf(currentHeadline);
  let nextHeadline = headlines[currentHeadlineIndex + 1];
  if (currentHeadlineIndex === headlines.length - 1) {
    nextHeadline = headlines[0];
  }

  dispatch({ type: HIDE_HEADLINE });

  setTimeout(() => {
    dispatch({ type: SHOW_NEXT_HEADLINE, payload: nextHeadline });
    dispatch({ type: SHOW_HEADLINE });
  }, 1000);
};

export const showNextTestimonial = () => (dispatch, getState) => {
  const testimonials = getTestimonials(getState());
  const currentTestimonial = getTestimonialToShow(getState());
  const currentTestimonialIndex = testimonials.indexOf(currentTestimonial);
  let nextTestimonial = testimonials[currentTestimonialIndex + 1];
  if (currentTestimonialIndex === testimonials.length - 1) {
    nextTestimonial = testimonials[0];
  }

  dispatch({ type: HIDE_TESTIMONIAL });

  setTimeout(() => {
    dispatch({ type: SHOW_NEXT_TESTIMONIAL, payload: nextTestimonial });
    dispatch({ type: SHOW_TESTIMONIAL });
  }, 1000);
};
