export default {
    en: {
        services: 'Our services',
        help: 'How can we help you?'
    },
    de: {
        services: 'Unsere Leistungen',
        help: 'Wie können wir dir helfen?'
    },
};
