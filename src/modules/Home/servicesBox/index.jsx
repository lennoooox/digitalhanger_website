import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'

// LOCALIZATION
import createLocalizedStrings from '../../../utils/createLocalizedStrings';
import localization from './localization';

// STYLES
import styles from './main.module.scss';
import {getLanguage} from '../../App/selectors';
import routes from '../../../routes';

const ServicesBox = props => {
    const {language} = props;
    const strings = createLocalizedStrings(language, localization);

    return (
        <section id={styles.services}>
            <div id={styles.content}>
                <Link to={routes.services.single.main.replace(':id', '1')} componentclass="p" id={styles.box}>
                    <p id={styles.link}>{strings.services}</p>
                </Link>
                <Link to={routes.services.single.main.replace(':id', '1')} componentclass="p"  id={styles.box}>
                    <p id={styles.link}>{strings.help}</p>
                </Link>
            </div>
        </section>
    );
};

ServicesBox.propTypes = {
    // mapStateToProps
    language: PropTypes.string,
};

const mapStateToProps = state => ({
    language: getLanguage(state),
});

const connected = connect(mapStateToProps)(ServicesBox);
export default connected;

