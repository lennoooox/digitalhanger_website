import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// SELECTORS
import { showNextTestimonial } from '../../actions';
// IMAGES
import ControlArrow from './images/control-arrow.svg';
// UI
import Fade from 'react-reveal/Fade';
// STYLES
import styles from './main.module.scss';

const Controls = ({ showNextTestimonial }) => {
  return (
    <Fade bottom distance="100px" delay={800} fraction={1}>
      <div id={styles.controls}>
        <div className={styles.controlButton}>
          <img src={ControlArrow} alt="" />
        </div>
        <div
          className={styles.controlButton}
          onClick={() => showNextTestimonial()}
        >
          <img src={ControlArrow} alt="" />
        </div>
      </div>
    </Fade>
  );
};

Controls.propTypes = {
  // mapDispatchToProps
  showNextTestimonial: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  showNextTestimonial: () => dispatch(showNextTestimonial()),
});

export default connect(
  null,
  mapDispatchToProps
)(Controls);
