import React from 'react';
import { connect } from 'react-redux';
import { Keyframes, animated, config } from 'react-spring';
// SELECTORS
import { getTestimonialHidden, getTestimonialToShow } from '../selectors';
// UI
import Author from './author';
import Pagination from './pagination';
import Controls from './controls';
import Fade from 'react-reveal/Fade';
// STYLES
import styles from './main.module.scss';

const AnimatedQuote = Keyframes.Spring({
  show: async next => {
    await next({ transform: 'translateX(50px)', opacity: 0 });
    await next({ transform: 'translateX(0px)', opacity: 1 });
  },
  hide: { transform: 'translateX(-50px)', opacity: 0 },
});

const Quotes = ({ testimonialToShow, testimonialHidden }) => {
  return (
      <section id={styles.quotes}>
        <div className="wrapper">
          <div id={styles.content}>
            <div className={styles.quote}>
              <Fade bottom distance="100px" delay={700} fraction={0.3}>
                <AnimatedQuote
                    native
                    config={config.stiff}
                    state={testimonialHidden ? 'hide' : 'show'}
                >
                  {props => (
                      <React.Fragment>
                        <animated.p style={props}>
                          {testimonialToShow.content}
                        </animated.p>
                        <Author style={props} testimonial={testimonialToShow} />
                      </React.Fragment>
                  )}
                </AnimatedQuote>
              </Fade>
              <div className={styles.controlsAndPagination}>
                <Pagination />
                <Controls />
              </div>
            </div>
          </div>
        </div>
      </section>
  );
};

const mapStateToProps = state => ({
  testimonialToShow: getTestimonialToShow(state),
  testimonialHidden: getTestimonialHidden(state),
});

export default connect(mapStateToProps)(Quotes);
