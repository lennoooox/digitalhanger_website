import React from 'react';
import { animated } from 'react-spring';
// STYLES
import styles from './main.module.scss';

const Author = ({ testimonial, style }) => (
  <animated.div className={styles.author} style={style}>
    <img
      src={testimonial.avatar}
      srcSet={`${testimonial.avatarRetina} 2x, ${testimonial.avatar} 1x`}
      alt=""
    />
    <div>
      <span>{testimonial.author}</span>
      <span>{testimonial.position}</span>
    </div>
  </animated.div>
);

export default Author;
