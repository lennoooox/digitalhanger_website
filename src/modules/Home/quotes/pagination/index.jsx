import React from 'react';
import { animated, Keyframes, config } from 'react-spring';
// UI
import Fade from 'react-reveal/Fade';
// STYLES
import styles from './main.module.scss';
import { getTestimonials, getTestimonialToShow } from '../../selectors';
import { connect } from 'react-redux';

const AnimatedDot = Keyframes.Spring({
  default: {
    boxShadow: 'inset 0px 0px 0px 0px transparent',
  },
  active: {
    boxShadow: 'inset 0px 0px 0px 2px #8E8EA1',
  },
});

const AnimatedIndicator = Keyframes.Spring({
  margin0: {
    transform: 'translateX(0px)',
  },
  margin1: {
    transform: 'translateX(36px)',
  },
  margin2: {
    transform: 'translateX(72px)',
  },
  margin3: {
    transform: 'translateX(108px)',
  },
});

const Pagination = ({ testimonials, testimonialToShow }) => {
  const indexOfTestimonial = testimonials.indexOf(testimonialToShow);

  return (
      <Fade bottom distance="100px" delay={1150} fraction={1}>
        <div className={styles.pagination}>
          <AnimatedIndicator
              native
              config={config.stiff}
              state={`margin${indexOfTestimonial}`}
          >
            {props => <animated.div className={styles.indicator} style={props} />}
          </AnimatedIndicator>
          {testimonials.map((_, index) => (
              <AnimatedDot
                  key={index}
                  native
                  state={indexOfTestimonial === index ? 'active' : 'default'}
                  config={config.stiff}
              >
                {props => <animated.div className={styles.dot} style={props} />}
              </AnimatedDot>
          ))}
        </div>
      </Fade>
  );
};

const mapStateToProps = state => ({
  testimonials: getTestimonials(state),
  testimonialToShow: getTestimonialToShow(state),
});

export default connect(mapStateToProps)(Pagination);