import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// ACTIONS
import {
    toggleFooter,
    toggleNavigationDark,
    toggleNavigationFullWidth,
} from '../App/actions';
// SELECTORS
import {
    getFooterVisible,
    getNavigationDark,
    getNavigationFullWidth,
} from '../App/selectors';
// UI
import Hero from './hero';
import Customers from './customers';
import Details from './details';
import Quotes from './quotes';
import ServicesBox from './servicesBox';
// STYLES
import styles from './main.module.scss';

class Home extends React.Component {
    componentDidMount() {
        const {
            footerVisible,
            navigationDark,
            navigationFullWidth,
            toggleFooter,
            toggleNavigationDark,
            toggleNavigationFullWidth,
            } = this.props;

        !footerVisible && toggleFooter(true);
        navigationDark && toggleNavigationDark(false);
        navigationFullWidth && toggleNavigationFullWidth(false);
    }
    render() {
        return (
            <main className={styles.home}>
                <Hero />
                <div className={styles.sectionHolder}>
                    <Customers />
                </div>
                <Details />
                <div className={styles.sectionHolder}>
                    <ServicesBox />
                </div>
            </main>
        );
    }
}

Home.propTypes = {
    // mapStateToProps
    footerVisible: PropTypes.bool.isRequired,
    navigationDark: PropTypes.bool.isRequired,
    navigationFullWidth: PropTypes.bool.isRequired,
    // mapDispatchToProps
    toggleFooter: PropTypes.func.isRequired,
    toggleNavigationDark: PropTypes.func.isRequired,
    toggleNavigationFullWidth: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    footerVisible: getFooterVisible(state),
    navigationDark: getNavigationDark(state),
    navigationFullWidth: getNavigationFullWidth(state),
});

const mapDispatchToProps = dispatch => ({
    toggleFooter: value => dispatch(toggleFooter(value)),
    toggleNavigationDark: value => dispatch(toggleNavigationDark(value)),
    toggleNavigationFullWidth: value =>
        dispatch(toggleNavigationFullWidth(value)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
