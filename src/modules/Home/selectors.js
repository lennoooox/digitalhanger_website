import { NAMESPACE } from './constants';

export const getHeadlineToShow = state => state[NAMESPACE].headlineToShow;
export const getHeadlines = state => state[NAMESPACE].headlines;
export const getHeadlineHidden = state => state[NAMESPACE].headlineHidden;

export const getTestimonialToShow = state => state[NAMESPACE].testimonialToShow;
export const getTestimonials = state => state[NAMESPACE].testimonials;
export const getTestimonialHidden = state => state[NAMESPACE].testimonialHidden;
