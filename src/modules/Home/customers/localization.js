export default {
    en: {
        headline: 'Together with our customers we create great things',
        paragraph: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculusmus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massaquis enim.',
        linkText: 'Contact now',
    },
    de: {
        headline: 'Gemeinsam mit unseren Kunden erschaffen wir Großes',
        paragraph: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculusmus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massaquis enim.',
        linkText: 'Jetzt Kontakt aufnehmen',
    },
};
