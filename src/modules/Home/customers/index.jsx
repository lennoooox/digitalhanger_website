import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
// LOCALIZATION
import createLocalizedStrings from '../../../utils/createLocalizedStrings';
import localization from './localization';
// STYLES
import styles from './main.module.scss';
import {getLanguage} from '../../App/selectors';

const CustomerBox = props => {
    const {language} = props;
    const strings = createLocalizedStrings(language, localization);

    return (
        <section>
            <div id={styles.box} style={props}>
                <div id={styles.background}></div>
                <div id={styles.wrapper}>
                    <p id={styles.headline}>{strings.headline}</p>
                    <p id={styles.paragraph}>{strings.paragraph}</p>
                    <a id={styles.link} href="">{strings.linkText}</a>
                </div>
            </div>
        </section>
    );
};

CustomerBox.propTypes = {
    // mapStateToProps
    language: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
    language: getLanguage(state),
});

const connected = connect(mapStateToProps)(CustomerBox);
export default connected;
