export default {
    de: {
        headlineShop: 'Dein Shop.',
        headlineLeads: 'Deine Leads.',
        headlineAgency: 'Deine Agentur.',
        paragraphIntroduction: 'Unser talentiertes Entwicklerkollektiv schafft durch innovative und neuartige Technologien digitale Erlebnisse für Menschen.',
        buttonText: 'Jetzt Umsatz steigern',
    },
    en: {
        headlineShop: 'Your Shop.',
        headlineLeads: 'Your Leads.',
        headlineAgency: 'Your Agency.',
        paragraphIntroduction: 'Our talented developer collective creates digital experiences for people through innovative and novel technologies.',
        buttonText: 'Increase sales now',
    },
};
