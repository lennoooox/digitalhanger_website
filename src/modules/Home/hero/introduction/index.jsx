import React from 'react';
import PropTypes from 'prop-types';
// import uniqueId from 'lodash/uniqueId';
import { connect } from 'react-redux';
import { Keyframes, animated } from 'react-spring';
// LOCALIZATION
import createLocalizedStrings from '../../../../utils/createLocalizedStrings';
import localization from './localization';
// ACTIONS
import { showNextHeadline } from '../../actions';
// SELECTORS
import {
    getHeadlineHidden,
    getHeadlines,
    getHeadlineToShow,
} from '../../selectors';
import { getLanguage } from '../../../App/selectors';
// UI
import Fade from 'react-reveal/Fade';
// STYLES
import styles from './main.module.scss';

const AnimatedHeadline = Keyframes.Spring({
    show: async next => {
        await next({transform: 'translateX(50px)', opacity: 0});
        await next({transform: 'translateX(0px)', opacity: 1});
    }
});

class Introduction extends React.Component {
    render() {
        const { language } = this.props;
        const strings = createLocalizedStrings(language, localization);

        return (
            <div id={styles.introduction}>
                <AnimatedHeadline native state={'show'}>
                    {props => (
                        <animated.h1
                            style={props}
                            className={styles.headline}
                            ref={this.headline}
                        >
                            {strings.headlineShop}
                            <br/>
                            {strings.headlineLeads}
                            <br/>
                            {strings.headlineAgency}
                        </animated.h1>
                    )}
                </AnimatedHeadline>
                <AnimatedHeadline native state={'show'}>
                    {props => (
                        <animated.p
                            style={props}
                            className={styles.paragraph}
                            ref={this.paragraph}
                        >
                            {strings.paragraphIntroduction}
                        </animated.p>
                    )}
                </AnimatedHeadline>
                <AnimatedHeadline native state={'show'}>
                    {props => (
                        <animated.button
                            style={props}
                            className={styles.button}
                            ref={this.button}
                        >
                            {strings.buttonText}
                        </animated.button>
                    )}
                </AnimatedHeadline>
            </div>
        );
    }
}

Introduction.propTypes = {
    // mapStateToProps
    language: PropTypes.string.isRequired,
    headlines: PropTypes.arrayOf(PropTypes.string).isRequired,
    // mapDispatchToProps
    showNextHeadline: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    language: getLanguage(state),
    headlines: getHeadlines(state),
    headlineHidden: getHeadlineHidden(state),
    headlineToShow: getHeadlineToShow(state),
});

const mapDispatchToProps = dispatch => ({
    showNextHeadline: () => dispatch(showNextHeadline()),
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps
)(Introduction);

export default connected;
