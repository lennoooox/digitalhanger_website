import React from 'react';
// UI
import Introduction from './introduction';
// STYLES
import styles from './main.module.scss';

const Hero = () => {
  return (
    <section id={styles.hero}>
      <div className="wrapper">
        <div id={styles.content}>
          <Introduction />
        </div>
      </div>
    </section>
  );
};

export default Hero;
