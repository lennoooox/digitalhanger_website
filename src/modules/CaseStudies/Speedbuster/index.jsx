import React from 'react';
// IMAGES
import ThreePhones from './images/3-phones.png';
import ThreePhonesRetina from './images/3-phones@2x.png';
// STYLES
import styles from './main.module.scss';

class Speedbuster extends React.Component {
  render() {
    return (
      <main className={styles.main}>
        <section id={styles.hero}>
          <div className="wrapper">
            <div className={styles.content}>
              <div className={styles.copy}>
                <h1>Schnell, schneller, Speedbuster</h1>
                <p>
                  Wer ein schnelles Auto fährt, will es meistens auch ausfahren.
                  Mit der Chiptuning-App von Speedbuster können Enthusiasten
                  ganz einfach ihre Chipbox kontrollieren und steuern, und somit
                  beliebig die Leistung des Motors anzupassen.
                </p>
              </div>
              <img
                src={ThreePhones}
                srcSet={`${ThreePhones} 1x, ${ThreePhonesRetina} 2x`}
                alt=""
              />
            </div>
          </div>
        </section>
        <section id={styles.goal}>
          <div className="wrapper">
            <div className={styles.content}>
              <h2>Das Ziel</h2>
              <p>
                Ein Werkzeug für Auto-Liebhaber. Dank moderner Technik
                ermöglicht Speedbuster mehr Kontrolle beim Chiptuning.
              </p>
            </div>
          </div>
        </section>
      </main>
    );
  }
}

export default Speedbuster;
