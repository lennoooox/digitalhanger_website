import { NAMESPACE } from './constants';

export const getLanguage = state => state[NAMESPACE].language;
export const getFooterVisible = state => state[NAMESPACE].footerVisible;
export const getFooterExtraNavigationVisible = state =>
  state[NAMESPACE].footerExtraNavigationVisible;
export const getOverlayVisible = state => state[NAMESPACE].overlayVisible;
export const getNavigationDark = state => state[NAMESPACE].navigationDark;
export const getNavigationFullWidth = state =>
  state[NAMESPACE].navigationFullWidth;
