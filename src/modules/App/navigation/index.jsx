import React from 'react';
import PropTypes from 'prop-types';
import routes from '../../../routes';
import { connect } from 'react-redux';
import cn from 'classnames';
// SELECTORS
import {
    getLanguage,
    getNavigationDark,
    getNavigationFullWidth,
    getOverlayVisible,
} from '../selectors';
// ACTIONS
import { toggleOverlay, updateLanguage } from '../actions';
// UI
import { Link } from 'react-router-dom';
// IMAGES
import LogoLight from './images/logo.svg';
import LogoDark from './images/logo.svg';
import LanguageToggleLight from './images/language-toggle-light.svg';
import LanguageToggleDark from './images/language-toggle-dark.svg';
import MenuToggleLight from './images/menu-toggle.svg';
import MenuToggleDark from './images/menu-toggle.svg';
// STYLES
import styles from './main.module.scss';

const Navigation = props => {
  const { dark, fullWidth, overlayVisible, toggleOverlay } = props;
  return (
      <nav id={styles.navigation}>
        <div className={cn('wrapper', { fullwidth: fullWidth })}>
          <div id={styles.content}>
            <div id={styles.logoPart}>
              <Link to={routes.main}>
                <img src={dark || overlayVisible ? LogoDark : LogoLight} alt="" />
              </Link>
            </div>
            <div id={styles.menuTogglePart}>
              <img
                  src={dark || overlayVisible ? MenuToggleDark : MenuToggleLight}
                  alt=""
                  onClick={
                overlayVisible
                  ? () => toggleOverlay(false)
                  : () => toggleOverlay(true)
              }
              />
            </div>
          </div>
        </div>
      </nav>
  );
};

Navigation.propTypes = {
  // mapStateToProps
  dark: PropTypes.bool.isRequired,
  fullWidth: PropTypes.bool.isRequired,
  // mapDispatchToProps
  toggleOverlay: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  dark: getNavigationDark(state),
  fullWidth: getNavigationFullWidth(state),
  overlayVisible: getOverlayVisible(state),
});

const mapDispatchToProps = dispatch => ({
  toggleOverlay: value => dispatch(toggleOverlay(value)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navigation);
