import {
  UPDATE_LANGUAGE,
  TOGGLE_FOOTER,
  TOGGLE_OVERLAY,
  TOGGLE_NAVIGATION_DARK,
  TOGGLE_NAVIGATION_FULL_WIDTH,
  TOGGLE_FOOTER_EXTRA_NAVIGATION,
} from './actions';

const initialState = {
  language: 'en',
  footerVisible: true,
  footerExtraNavigationVisible: false,
  overlayVisible: false,
  navigationDark: false,
  navigationFullWidth: false,
};

const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_LANGUAGE:
      return Object.assign({}, state, {
        language: action.payload,
      });
    case TOGGLE_FOOTER:
      return Object.assign({}, state, {
        footerVisible: action.payload,
      });
    case TOGGLE_FOOTER_EXTRA_NAVIGATION:
      return Object.assign({}, state, {
        footerExtraNavigationVisible: action.payload,
      });
    case TOGGLE_OVERLAY:
      return Object.assign({}, state, {
        overlayVisible: action.payload,
      });
    case TOGGLE_NAVIGATION_DARK:
      return Object.assign({}, state, {
        navigationDark: action.payload,
      });
    case TOGGLE_NAVIGATION_FULL_WIDTH:
      return Object.assign({}, state, {
        navigationFullWidth: action.payload,
      });
    default:
      return state;
  }
};

export default AppReducer;
