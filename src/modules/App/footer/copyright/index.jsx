import React from 'react';

// STYLES
import styles from './main.module.scss';

const Copyright = () => {
    return (
        <div>
            <hr id={styles.hr}/>
            <div id={styles.content}>
                <br/>
                <div id={styles.links}>
                    <div id={styles.copy}>
                        <p>Copyright (c) 2019 – Digitalhangar</p>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default (Copyright);
