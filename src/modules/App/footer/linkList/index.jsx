import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';
import uniqueId from 'lodash/uniqueId';
// UI
import { Link } from 'react-router-dom';
// STYLES
import styles from './main.module.scss';

const LinkList = ({ dark, headline, links }) => {
  return (
    <div id={styles.list} className={cn({ [styles.dark]: dark })}>
      <h3 className={styles.headline}>{headline}</h3>
      <ul>
        {links.map(link => (
          <li key={uniqueId('navigation-overlay-legal-link-')}>
            <Link to={link.url}>{link.label}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

LinkList.linkShape = {
  label: PropTypes.string,
  url: PropTypes.string,
};

LinkList.propTypes = {
  dark: PropTypes.bool,
  headline: PropTypes.string.isRequired,
  links: PropTypes.arrayOf(PropTypes.shape(LinkList.linkShape)).isRequired,
};

LinkList.defaultProps = {
  dark: false,
};

export default LinkList;
