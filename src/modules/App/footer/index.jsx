import React from 'react';
import { connect } from 'react-redux';
// SELECTORS
import { getFooterExtraNavigationVisible } from '../selectors';
// IMAGES
import Logo from './images/logo.svg';
// UI
import Offices from './offices';
import Contact from './contact';
import LegalLinks from '../overlay/legalLinks';
import NavigationLinks from './navigationLinks';
import LanguageSwitch from './languageSwitch';
import Copyright from './copyright';
// STYLES
import styles from './main.module.scss';

const Footer = () => (
    <footer id={styles.footer}>
      <div className="wrapper">
        <div id={styles.content}>
          <div id={styles.logo}>
            <img src={Logo} alt="" />
            <LanguageSwitch />
          </div>
          <Offices />
          <Contact />
          <NavigationLinks />
          <LegalLinks />
        </div>
        <Copyright />
      </div>
    </footer>
);

function mapStateToProps(state) {
  return {
    footerExtraNavigationVisible: getFooterExtraNavigationVisible(state),
  };
}

export default connect(mapStateToProps)(Footer);
