export default {
  en: {
    contact: 'Contact',
    operativeCEO: 'Operative CEO',
    founderAndCEO: 'Founder and CEO',
  },
  de: {
    contact: 'Kontakt',
    operativeCEO: 'Operativer Geschäftsführer',
    founderAndCEO: 'Gründer und Geschäftsführer',
  },
};
