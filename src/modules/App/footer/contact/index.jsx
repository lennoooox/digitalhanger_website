import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { connect } from 'react-redux';
// LOCALIZATION
import createLocalizedStrings from '../../../../utils/createLocalizedStrings';
import localization from './localization';
// SELECTORS
import { getLanguage } from '../../selectors';
// IMAGES
import PatrickMoeller from './images/patrick-moeller.jpg';
import PatrickMoellerRetina from './images/patrick-moeller@2x.jpg';
// STYLES
import styles from './main.module.scss';

const Contact = props => {
  const { language, dark } = props;
  const strings = createLocalizedStrings(language, localization);

  return (
    <div className={cn({ [styles.dark]: dark })}>
      <h3 className={styles.headline}>{strings.contact}</h3>
      <address className={styles.card}>
        <img
          src={PatrickMoeller}
          srcSet={`${PatrickMoellerRetina} 2x, ${PatrickMoeller} 1x`}
          alt=""
        />
        Patrick Möller <br />
        {strings.operativeCEO} <br />
        patrick@kiai.de <br />
        +49 157 71366 141
      </address>
    </div>
  );
};

Contact.propTypes = {
  dark: PropTypes.bool,
  // mapStateToProps
  language: PropTypes.string.isRequired,
};

Contact.defaultProps = {
  dark: false
};

const mapStateToProps = state => ({
  language: getLanguage(state)
});

export default connect(mapStateToProps)(Contact);
