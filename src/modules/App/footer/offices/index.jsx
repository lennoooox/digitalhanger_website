import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// LOCALIZATION
import createLocalizedStrings from '../../../../utils/createLocalizedStrings';
import localization from './localization';
// SELECTORS
import { getLanguage } from '../../selectors';
// STYLES
import styles from './main.module.scss';

const Offices = props => {
  const { language, dark } = props;
  const strings = createLocalizedStrings(language, localization);

  return (
    <div className={dark && styles.dark}>
      <h3 className={styles.headline}>{strings.offices}</h3>
      <address className={styles.address}>
        Frankfurt <br />
        Europa-Allee 140-142 <br />
        60586 Frankfurt am Main <br />
        {strings.germany}
      </address>
    </div>
  );
};

Offices.propTypes = {
  // mapStateToProps
  language: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  language: getLanguage(state),
});

export default connect(mapStateToProps)(Offices);
