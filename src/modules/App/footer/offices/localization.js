export default {
  en: {
    offices: 'Offices',
    germany: 'Germany',
  },
  de: {
    offices: 'Büros',
    germany: 'Deutschland',
  },
};
