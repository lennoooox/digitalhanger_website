import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// SELECTORS
import { getLanguage } from '../../selectors';
// LOCALIZATION
import localization from '../../overlay/navigation/localization';
import createLocalizedStrings from '../../../../utils/createLocalizedStrings';
// UI
import LinkList from '../../footer/linkList';
import routes from '../../../../routes';

const NavigationLinks = ({ dark, language }) => {
  const strings = createLocalizedStrings(language, localization);
  const links = [
    {
      label: strings.home,
      url: routes.main,
    },
    {
      label: strings.services,
      url: routes.services.main,
    },
    {
      label: strings.contact,
      url: routes.contact.main,
    },
  ];

  return <LinkList dark={dark} headline={strings.headline} links={links} />;
};

NavigationLinks.propTypes = {
  dark: PropTypes.bool,
  // mapStateToProps
  language: PropTypes.string.isRequired,
};

NavigationLinks.defaultProps = {
  dark: false,
};

const mapStateToProps = state => ({
  language: getLanguage(state),
});

export default connect(mapStateToProps)(NavigationLinks);
