import React from 'react';
import cn from 'classnames';
import { connect } from 'react-redux';
// SELECTORS
import { getLanguage } from '../../selectors';
// ACTIONS
import { updateLanguage } from '../../actions';
// STYLES
import styles from './main.module.scss';

const LanguageSwitch = props => {
  const { dark, language, updateLanguage } = props;

  return (
    <div
      className={styles.languageSwitch}
      className={cn(styles.languageSwitch, { [styles.dark]: dark })}
    >
      <button
        onClick={() => updateLanguage('de')}
        className={cn(styles.button, { [styles.active]: language === 'de' })}
      >
        DE
      </button>
      <button
        onClick={() => updateLanguage('en')}
        className={cn(styles.button, { [styles.active]: language === 'en' })}
      >
        EN
      </button>
    </div>
  );
};

const mapStateToProps = state => ({
  language: getLanguage(state),
});

const mapDispatchToProps = dispatch => ({
  updateLanguage: lang => dispatch(updateLanguage(lang)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LanguageSwitch);
