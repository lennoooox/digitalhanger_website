export const UPDATE_LANGUAGE = 'APP/UPDATE_LANGUAGE';
export const TOGGLE_FOOTER = 'APP/TOGGLE_FOOTER';
export const TOGGLE_OVERLAY = 'APP/TOGGLE_OVERLAY';
export const TOGGLE_NAVIGATION_DARK = 'APP/TOGGLE_NAVIGATION_DARK';
export const TOGGLE_NAVIGATION_FULL_WIDTH = 'APP/TOGGLE_NAVIGATION_FULL_WIDTH';
export const TOGGLE_FOOTER_EXTRA_NAVIGATION =
  'APP/TOGGLE_FOOTER_EXTRA_NAVIGATION';

export const updateLanguage = languageKey => dispatch => {
  return dispatch({
    type: UPDATE_LANGUAGE,
    payload: languageKey,
  });
};

export const toggleFooter = value => dispatch => {
  return dispatch({
    type: TOGGLE_FOOTER,
    payload: value,
  });
};

export const toggleFooterExtraNavigation = value => dispatch => {
  return dispatch({
    type: TOGGLE_FOOTER_EXTRA_NAVIGATION,
    payload: value,
  });
};

export const toggleOverlay = value => dispatch => {
  return dispatch({
    type: TOGGLE_OVERLAY,
    payload: value,
  });
};

export const toggleNavigationDark = value => dispatch => {
  return dispatch({
    type: TOGGLE_NAVIGATION_DARK,
    payload: value,
  });
};

export const toggleNavigationFullWidth = value => dispatch => {
  return dispatch({
    type: TOGGLE_NAVIGATION_FULL_WIDTH,
    payload: value,
  });
};
