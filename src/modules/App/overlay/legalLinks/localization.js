export default {
  en: {
    headline: 'Legal',
    imprint: 'Imprint',
    termsOfServices: 'Terms of Services',
    privacyPolicy: 'Privacy Policy',
  },
  de: {
    headline: 'Rechtliches',
    imprint: 'Impressum',
    termsOfServices: 'AGB',
    privacyPolicy: 'Datenschutz',
  },
};
