import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// SELECTORS
import { getLanguage } from '../../selectors';
// LOCALIZATION
import localization from './localization';
// UI
import LinkList from '../../footer/linkList';
import createLocalizedStrings from '../../../../utils/createLocalizedStrings';
import routes from '../../../../routes';

const LegalLinks = ({ dark, language }) => {
  const strings = createLocalizedStrings(language, localization);
  const links = [
    {
      label: strings.imprint,
      url: routes.imprint.main,
    },
  ];

  return <LinkList dark={dark} headline={strings.headline} links={links} />;
};

LegalLinks.propTypes = {
  dark: PropTypes.bool,
  // mapStateToProps
  language: PropTypes.string.isRequired,
};

LegalLinks.defaultProps = {
  dark: false,
};

const mapStateToProps = state => ({
  language: getLanguage(state),
});

export default connect(mapStateToProps)(LegalLinks);
