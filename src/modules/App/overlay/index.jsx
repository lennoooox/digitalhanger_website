import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Keyframes, animated } from 'react-spring';
// SELECTORS
import { getOverlayVisible } from '../selectors';
// UI
import Navigation from './navigation';
import Offices from '../footer/offices';
import Contact from '../footer/contact';
import LegalLinks from './legalLinks';
// STYLES
import styles from './main.module.scss';

const hiddenStyles = {
  height: '0%',
};

const visibleStyles = {
  height: '100%',
};

const AnimatedOverlay = Keyframes.Spring({
  show: {
    from: hiddenStyles,
    to: visibleStyles,
  },
  hide: {
    to: hiddenStyles,
  },
});

const Overlay = ({ overlayVisible }) => {
  return (
    <AnimatedOverlay native state={overlayVisible ? 'show' : 'hide'}>
      {props => (
        <animated.div style={props} id={styles.overlay}>
          <div className="wrapper">
            <div id={styles.content}>
              <Navigation />
              <Offices dark />
              <Contact dark />
              <LegalLinks dark />
            </div>
          </div>
        </animated.div>
      )}
    </AnimatedOverlay>
  );
};

Overlay.propTypes = {
  overlayVisible: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  overlayVisible: getOverlayVisible(state),
});

export default connect(mapStateToProps)(Overlay);
