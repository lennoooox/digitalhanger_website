import React from 'react';
import PropTypes from 'prop-types';
import routes from '../../../../routes';
import { connect } from 'react-redux';
// LOCALIZATION
import createLocalizedStrings from '../../../../utils/createLocalizedStrings';
import localization from './localization';
// ACTIONS
import { toggleOverlay } from '../../actions';
// UI
import { NavLink } from 'react-router-dom';
import LanguageSwitch from '../../footer/languageSwitch';
// STYLES
import styles from './main.module.scss';
import { getLanguage } from '../../selectors';

const Navigation = ({ toggleOverlay, language }) => {
  const strings = createLocalizedStrings(language, localization);

  return (
    <nav id={styles.navigation}>
      <ul>
        <li>
          <NavLink onClick={() => toggleOverlay(false)} to={routes.main}>
            {strings.home}
          </NavLink>
        </li>
        <li>
          <NavLink
            onClick={() => toggleOverlay(false)}
            to={routes.services.main}
          >
            {strings.services}
          </NavLink>
        </li>
        <li>
          <NavLink
            onClick={() => toggleOverlay(false)}
            to={routes.contact.main}
          >
            {strings.contact}
          </NavLink>
        </li>
      </ul>
      <LanguageSwitch dark />
    </nav>
  );
};

Navigation.propTypes = {
  // mapDispatchToProps
  toggleOverlay: PropTypes.func.isRequired,
  language: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  language: getLanguage(state),
});

const mapDispatchToProps = dispatch => ({
  toggleOverlay: value => dispatch(toggleOverlay(value)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navigation);
