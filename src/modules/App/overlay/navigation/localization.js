export default {
  en: {
    headline: 'Navigation',
    home: 'Home',
    services: 'Services',
    projects: 'Projects',
    culture: 'Culture',
    contact: 'Contact',
  },
  de: {
    home: 'Home',
    services: 'Leistungen',
    projects: 'Projekte',
    culture: 'Kultur',
    contact: 'Kontakt',
  },
};
