import React from 'react';
import PropTypes from 'prop-types';
import routes from '../../routes';
import { connect } from 'react-redux';
// SELECTORS
import { getFooterVisible } from './selectors';
// ROUTER
import { ConnectedRouter } from 'connected-react-router';
import { Switch, Route, Redirect } from 'react-router-dom';
// PAGES
import Home from '../Home';
import Services from '../Services';
import Contact from '../Contact';
import Imprint from '../Imprint';
// UI
import ScrollToTop from '../HOCS/ScrollToTop';
import Navigation from './navigation';
import Overlay from './overlay';
import Footer from './footer';

const App = props => {
  const { history, footerVisible } = props;
  return (
      <ConnectedRouter history={history}>
        <ScrollToTop>
          <Navigation />
          <Overlay />
          <Switch>
            <Route exact path={routes.main} component={Home} />
            <Route
                exact
                path={routes.services.single.main}
                component={Services}
            />
            <Route
                expact
                path={routes.services.main}
                render={() => (
              <Redirect to={routes.services.single.main.replace(':id', 1)} />
            )}
            />
            <Route exact path={routes.contact.main} component={Contact} />
            <Route exact path={routes.imprint.main} component={Imprint} />
            <Route render={() => <Redirect to={routes.main} />} />
          </Switch>
          {footerVisible && <Footer />}
        </ScrollToTop>
      </ConnectedRouter>
  );
};

App.propTypes = {
  history: PropTypes.object.isRequired,
  footerVisible: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  footerVisible: getFooterVisible(state),
});

export default connect(mapStateToProps)(App);
