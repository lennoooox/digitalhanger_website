import React from 'react';
import PropTypes from 'prop-types';
import uniqueId from 'lodash/uniqueId';
import { connect } from 'react-redux';
import { Keyframes, animated } from 'react-spring';
import Logos from './logos';
// SELECTORS
import { getAnimating, getShownServiceId } from '../selectors';
// ACTIONS
import { setShownService } from '../actions';
// LOCALIZATION
import createLocalizedStrings from '../../../utils/createLocalizedStrings';
import localization from '../localization';
// STYLES
import styles from './main.module.scss';
import { getLanguage } from '../../App/selectors';
import { withRouter } from 'react-router';

const DetailsContainer = Keyframes.Spring({
  show: {
    opacity: 1,
  },
  hide: {
    opacity: 0,
  },
});

const Details = ({ animating, shownServiceId, language, match }) => {
  const strings = createLocalizedStrings(language, localization);
  const serviceAligned = match.params.id === shownServiceId;

  if (!animating && !serviceAligned) setShownService(match.params.id);

  return (
    <section id={styles.details}>
      <DetailsContainer native state={animating ? 'hide' : 'show'}>
        {props => (
          <animated.div
            style={{ ...props, width: '100%', position: 'relative' }}
          >
            <span className={styles.number}>{`0${shownServiceId}`}</span>
            <span className={styles.title}>
              {strings[`service${shownServiceId}`].title}
            </span>
            <h1 className={styles.quote}>
              {strings[`service${shownServiceId}`].quote}
            </h1>
            <span className={styles.sectionTitle}>{strings.description}</span>
            <p className={styles.description}>
              {strings[`service${shownServiceId}`].desc}
            </p>
            <span className={styles.sectionTitle}>{strings.references}</span>
            <div className={styles.clients}>
              {strings[`service${shownServiceId}`].logos.map(logo => (
                <div key={uniqueId('company-logo-')}>
                  <img src={Logos[logo]} alt="" />
                </div>
              ))}
            </div>
          </animated.div>
        )}
      </DetailsContainer>
    </section>
  );
};

Details.propTypes = {
  // mapStateToProps
  language: PropTypes.string.isRequired,
  animating: PropTypes.bool.isRequired,
  shownServiceId: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
  language: getLanguage(state),
  animating: getAnimating(state),
  shownServiceId: getShownServiceId(state),
});

const mapDispatchToProps = dispatch => ({
  setShownService: id => dispatch(setShownService(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Details));
