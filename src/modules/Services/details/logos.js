import AEG from './images/aeg.svg';
import Sport1 from './images/sport1.svg';
import Safran from './images/safran.svg';
import WeltN24 from './images/weltn24.svg';
import Edel from './images/edel.svg';
import ADG from './images/adg.svg';
import DeutscheBank from './images/deutsche-bank.svg';
import SushiPalace from './images/sushi-palace.svg';
import Leica from './images/leica.svg';
import Canyon from './images/canyon.svg';
import Telekom from './images/telekom.svg';
import Speedbuster from './images/speedbuster.svg';
import Adidas from './images/adidas.svg';

export default {
  aeg: AEG,
  sport1: Sport1,
  safran: Safran,
  weltn24: WeltN24,
  edel: Edel,
  adg: ADG,
  deutscheBank: DeutscheBank,
  sushiPalace: SushiPalace,
  leica: Leica,
  canyon: Canyon,
  telekom: Telekom,
  speedbuster: Speedbuster,
  adidas: Adidas,
};
