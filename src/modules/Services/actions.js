export const TOGGLE_ANIMATING = 'SERVICES/TOGGLE_ANIMATING';
export const UPDATE_SHOWN_SERVICE = 'SERVICES/UPDATE_SHOWN_SERVICE';

export const toggleAnimating = value => dispatch => {
  dispatch({ type: TOGGLE_ANIMATING, payload: value });
};

export const setShownService = id => dispatch => {
  dispatch({
    type: UPDATE_SHOWN_SERVICE,
    payload: id,
  });
};

export const updateShownService = id => dispatch => {
  dispatch(toggleAnimating(true));

  setTimeout(() => {
    dispatch({ type: UPDATE_SHOWN_SERVICE, payload: id });
    dispatch(toggleAnimating(false));
  }, 500);
};
