export default {
  en: {
    overviewHeadline: 'Services',
    description: 'Description',
    references: 'References',
    service1: {
      title: 'Online-Shops',
      quote: 'We’ll glady consult you and support you throughout your journey.',
      desc:
        'Consultation is the first step in building a great product. In close collaboration with our clients we try to understand problems. Then we build a product with actual business value.',
      logos: ['edel', 'sushiPalace'],
    },
    service2: {
      title: 'Marketing',
      quote:
        'As your partner, we will find the perfect solution for the mobile App to your product.',
      desc:
        'Bei uns steht die detailgetreue Umsetzung funktionaler Meisterwerke unangefochten im Vordergrund. Zusammen mit unseren Kunden entwickeln wir deshalb die digitalen Lösungen für die Zukunft.',
      logos: [
        'telekom',
        'deutscheBank',
        'aeg',
        'weltn24',
        'adg',
        'speedbuster',
      ],
    },
    service3: {
      title: 'Leads',
      quote:
        'We find the most important customer touchpoints and then create experiences.',
      desc:
        'We develop cross-media. Whether on mobile or desktop, we provide solutions for your customers on all of today’s platforms. Responsive design is of course a pre-requisite.',
      logos: ['canyon', 'sushiPalace'],
    }
  },
  de: {
    overviewHeadline: 'Leistungen',
    description: 'Beschreibung',
    references: 'Referenzen',
    service1: {
      title: 'Beratung und Strategie',
      quote: 'Wir beraten Sie gerne und begleiten Sie auf Ihrem Weg.',
      desc:
        'Beratung ist der erste Schritt zu einem gelungenen Produkt. In enger Zusammenarbeit mit unseren Kunden versuchen wir Probleme zu verstehen um dann ein Produkt mit Business Value zu bauen.',
    },
    service2: {
      title: 'Native und hybride Apps',
      quote:
        'Als Ansprechpartner für die Mobile App zu Ihrem Produkt finden wir die passende Lösung.',
      desc:
        'Bei uns steht die detailgetreue Umsetzung funktionaler Meisterwerke unangefochten im Vordergrund. Zusammen mit unseren Kunden entwickeln wir deshalb die digitalen Lösungen für die Zukunft.',
    },
    service3: {
      title: 'Websites und Apps',
      quote:
        'Kunden dort abholen, wo Sie sich aufhalten und dann Erlebnisse schaffen.',
      desc:
        'Wir entwickeln cross-medial. Ob mobil oder auf dem Desktop unterwegs, wir bieten für Ihre Kunden auf jeder Plattform eine Lösung. Responsive Gestaltung steht dabei natürlich im Vordergrund.',
    }
  },
};
