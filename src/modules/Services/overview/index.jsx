import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { Keyframes, animated } from 'react-spring';
import { services } from '../constants';
// LOCALIZATION
import createLocalizedStrings from '../../../utils/createLocalizedStrings';
import localization from '../localization';
// UI
import { NavLink } from 'react-router-dom';
// STYLES
import styles from './main.module.scss';
import { updateShownService } from '../actions';
import { connect } from 'react-redux';
import { getLanguage } from '../../App/selectors';
import { getShownServiceId } from '../selectors';

const defaultStyles = {
  background: '#A2A2A2',
  WebkitTextFillColor: 'transparent',
};

const activeStyles = {
  background: '#000000',
  WebkitTextFillColor: 'transparent',
};

const ServiceLabel = Keyframes.Spring({
  active: activeStyles,
  default: defaultStyles,
});

const Overview = ({ updateShownService, shownServiceId, language, match }) => {
  const strings = createLocalizedStrings(language, localization);
  return (
    <section id={styles.overview}>
      <nav id={styles.overviewList}>
        <h1 className={styles.headline}>{strings.overviewHeadline}</h1>
        <ul>
          {services.map((list, listIndex) =>
            list.map((item, itemIndex) => {
              const itemActive = shownServiceId === item.id;
              const itemNumber = listIndex * 3 + itemIndex + 1;
              return (
                <li key={item.key}>
                  <ServiceLabel
                    native
                    state={itemActive ? 'active' : 'default'}
                  >
                    {props => (
                      <NavLink
                        to={item.url}
                        onClick={() => updateShownService(itemNumber)}
                      >
                        <small>{`0${itemNumber}`}</small>
                        <animated.span style={props}>
                          {strings[`service${itemNumber}`].title}
                        </animated.span>
                      </NavLink>
                    )}
                  </ServiceLabel>
                </li>
              );
            })
          )}
        </ul>
      </nav>
    </section>
  );
};

Overview.propTypes = {
  // mapStateToProps
  language: PropTypes.string.isRequired,
  shownServiceId: PropTypes.number.isRequired,
  // mapDispatchToProps
  updateShownService: PropTypes.func.isRequired,
  // HOC
  match: PropTypes.object,
};

const mapStateToProps = state => ({
  language: getLanguage(state),
  shownServiceId: getShownServiceId(state),
});

const mapDispatchToProps = dispatch => ({
  updateShownService: id => dispatch(updateShownService(id)),
});

const routed = withRouter(Overview);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(routed);
