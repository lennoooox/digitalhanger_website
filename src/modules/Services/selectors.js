import { NAMESPACE } from './constants';

export const getAnimating = state => state[NAMESPACE].animating;
export const getShownServiceId = state => state[NAMESPACE].shownService;
