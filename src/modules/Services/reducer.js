import { TOGGLE_ANIMATING, UPDATE_SHOWN_SERVICE } from './actions';

const initialState = {
  animating: false,
  shownService: 1,
};

const ServicesReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_ANIMATING:
      return Object.assign({}, state, {
        animating: action.payload,
      });
    case UPDATE_SHOWN_SERVICE:
      return Object.assign({}, state, {
        shownService: action.payload,
      });
    default:
      return state;
  }
};

export default ServicesReducer;
