import uniqueId from 'lodash/uniqueId';

export const NAMESPACE = 'services';

export const services = [
  [
    {
      id: 1,
      key: uniqueId('service-item-'),
      label: '{service}',
      url: '/services/1',
    },
    {
      id: 2,
      key: uniqueId('service-item-'),
      label: '{service}',
      url: '/services/2',
    },
    {
      id: 3,
      key: uniqueId('service-item-'),
      label: '{service}',
      url: '/services/3',
    }
  ]
];
