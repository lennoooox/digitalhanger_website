import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// SELECTORS
import { getFooterVisible, getNavigationDark } from '../App/selectors';
// ACTIONS
import { toggleFooter, toggleNavigationDark } from '../App/actions';
// UI
import Overview from './overview';
import Details from './details';
// STYLES
import styles from './main.module.scss';

class Services extends React.Component {
  componentDidMount() {
    const {
        footerVisible,
        navigationDark,
        toggleFooter,
        toggleNavigationDark,
        } = this.props;

    if (window.matchMedia('(min-width: 1200px)').matches) {
      footerVisible && toggleFooter(false);
    }

    navigationDark && toggleNavigationDark(false);

    window.onresize = () => {
      console.log('Resized!');
      if (window.matchMedia('(min-width: 1200px)').matches) {
        toggleFooter(false);
      } else {
        toggleFooter(true);
      }
    };
  }

  render() {
    return (
        <main id={styles.services}>
          <Overview />
          <Details />
        </main>
    );
  }
}

Services.propTypes = {
  // mapStateToProps
  language: PropTypes.string.isRequired,
  footerVisible: PropTypes.bool.isRequired,
  navigationDark: PropTypes.bool.isRequired,
  // mapDispatchToProps
  toggleFooter: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  footerVisible: getFooterVisible(state),
  navigationDark: getNavigationDark(state),
});

const mapDispatchToProps = dispatch => ({
  toggleFooter: value => dispatch(toggleFooter(value)),
  toggleNavigationDark: value => dispatch(toggleNavigationDark(value)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Services);
