import LocalizedStrings from 'react-localization';

export default (language, inputStrings) => {
  const strings = new LocalizedStrings(inputStrings);
  strings.setLanguage(language);
  return strings;
};
