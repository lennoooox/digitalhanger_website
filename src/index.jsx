import React from 'react';
import ReactDOM from 'react-dom';
import WebFont from 'webfontloader';
// REDUX
import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import rootReducer from './reducer';
// STYLES
import './global.scss';
// APP COMPONENT
import App from './modules/App';

WebFont.load({
  google: {
    families: ['IBM Plex Sans:300,600,900'],
  }
});

const history = createBrowserHistory();
const initialState = {};
const enhancers = [];
const middleware = [thunk, promise, routerMiddleware(history)];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;
  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

const store = createStore(
  rootReducer(history),
  initialState,
  composedEnhancers
);

ReactDOM.render(
  <Provider store={store}>
    <App history={history} />
  </Provider>,
  document.getElementById('root')
);
