import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
// REDUCERS
import AppReducer from './modules/App/reducer';
import { NAMESPACE as AppNamespace } from './modules/App/constants';
import HomeReducer from './modules/Home/reducer';
import { NAMESPACE as HomeNamespace } from './modules/Home/constants';
import ServicesReducer from './modules/Services/reducer';
import { NAMESPACE as ServicesNamespace } from './modules/Services/constants';
import ContactReducer from './modules/Contact/reducer';
import { NAMESPACE as ContactNamespace } from './modules/Contact/constants';

export default history =>
  combineReducers({
    router: connectRouter(history),
    [AppNamespace]: AppReducer,
    [HomeNamespace]: HomeReducer,
    [ServicesNamespace]: ServicesReducer,
    [ContactNamespace]: ContactReducer,
  });
