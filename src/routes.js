export default {
    main: '/',
    services: {
        main: '/services',
        single: {
            main: '/services/:id',
        },
    },
    contact: {
        main: '/contact',
    },
    imprint: {
        main: '/imprint',
    },
};
